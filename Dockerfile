FROM node
COPY ./calendar .
RUN npm install
RUN npm install -g @angular/cli
EXPOSE 4200
CMD [ "ls" ]
CMD [ "ng", "serve" ]
