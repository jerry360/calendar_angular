# Calendar by Jernej Zupančič

## First run
### Requirements:
* docker

### Setup:
```
docker build . -t calendar-app
docker images
```

## Run:

```
docker run -p 4200:4200 -d calendar-app
```
