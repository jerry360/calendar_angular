import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Holiday, HolidayService } from 'src/app/services/holiday.service';
import { Day } from '../main.component'

@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.scss']
})
export class DayComponent implements OnInit {

  @Input() today!: Day;
  
  public holidaysToday:Array<Holiday>=new Array()
  public is_today:boolean=false;


  constructor(private holidayService:HolidayService) { 
  }

  ngOnInit(): void {
    if(this.today!=undefined){
      this.holidaysToday=this.holidayService.returnHolidayForDay(this.today.date)

      if(moment().format('YYYY-M-D')===this.today.date){
        console.log("isti")
        this.is_today=true;
      }
    }
  }

}
