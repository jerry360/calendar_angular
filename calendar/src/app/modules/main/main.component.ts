import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})



export class MainComponent implements OnInit {

  public months:Array<string> = ['January','February','March','April','May','June','July','August','September','October','November','December']
  public days:Array<string> = ['MON','TUE','WED','THU','FRI','SAT','SUN']
  public now = moment().calendar;

  public current_month:string = '';
  public current_year:number = 0;
  public current_day:number = 0;

  public days_in_month:Array<Day>=new Array();
  public weeks_in_month:Array<Array<Day>>=new Array();

  constructor() {
    console.log(this.now);
    this.current_month = this.months[moment().month()];
    this.current_year = moment().year();
    this.current_day = moment().day();
    this.changeView();
  }

  ngOnInit(): void {
  }


  //za vsak dan v mesecu shranim datum in ime dneva
  private returnDaysOfMonth(_selected_m:moment.Moment,_year:number,_month:number):Array<Day>{

    let tmpArrayOfDays:Array<Day>=new Array()
    
    for (var _i = 1; _i < _selected_m.daysInMonth()+1 ; _i++) {
      let date=(_year)+"-"+(_month)+"-"+(_i)
      let tmp_day=new Day(_i,date, moment(date,'YYYY-MM-DD').format('ddd'))
      tmpArrayOfDays.push(tmp_day)
    }
    return tmpArrayOfDays
  }


  public changeView(){
    //brisem stare datume
    this.days_in_month=new Array();
    this.weeks_in_month=new Array();

    console.log(this.current_day,this.current_month,this.current_year)

    //za izbran mesec najdem dneve
    let selected_m=moment(this.current_year+"-"+(this.months.indexOf(this.current_month)+1), "YYYY-M")
    this.days_in_month=this.returnDaysOfMonth(selected_m, this.current_year, this.months.indexOf(this.current_month)+1)

    //najdem kako se imenuje prvi in zadnji dan v mesecu
    let first_of_the_month=selected_m.startOf('month').format("ddd").toLocaleUpperCase()
    let last_of_the_month=selected_m.endOf('month').format("ddd").toLocaleUpperCase()

    //če prvi dan v mesecu ni ponedeljek dodaj dnevom v mesecu tudi dneve iz prejšnega meseca
    if(first_of_the_month!=="MON"){

      let days_previus_month:Array<Day>=new Array();
      let selected_m_back=moment()

      //najdem prejsni mesec
      if((this.months.indexOf(this.current_month)+1)==1){
        //ce je prvi moram iti eno leto nazaj
        let selected_year_back=this.current_year-1;
        selected_m_back=moment(selected_year_back+"-12", "YYYY-M")
        days_previus_month=this.returnDaysOfMonth(selected_m_back,selected_year_back,12);
        days_previus_month.forEach((_day:Day)=>{_day.not_in_curent_month=true});

      }else{
        //ce ni januar
        let month_before=(this.months.indexOf(this.current_month))
        selected_m_back=moment(this.current_year+"-"+month_before, "YYYY-M")
        days_previus_month=this.returnDaysOfMonth(selected_m_back,this.current_year,month_before);
        days_previus_month.forEach((_day:Day)=>{_day.not_in_curent_month=true});
      }

      for (var _i = selected_m_back.daysInMonth()-1; _i > 1  ; _i--) {
        this.days_in_month.unshift(days_previus_month[_i]) 
        if(days_previus_month[_i].name==="Mon"){
          break;
        }
      }
    }

    //če zadnji dan v mesecu ni nedelja dodaj dnevom v mesecu tudi dneve iz naslednjega meseca
    if(last_of_the_month!=="SUN"){

      let days_next_month:Array<Day>=new Array();
      let selected_m_forward=moment()

      //najdem naslednji mesec
      if((this.months.indexOf(this.current_month)+1)==12){
        //ce je december moram iti eno leto naprej
        let selected_year_next=this.current_year+1;
        selected_m_forward=moment(selected_year_next+"-1", "YYYY-M")
        days_next_month=this.returnDaysOfMonth(selected_m_forward,selected_year_next,1);
        days_next_month.forEach((_day:Day)=>{_day.not_in_curent_month=true});

      }else{
        //ce ni december
        let month_forward=(this.months.indexOf(this.current_month)+2)
        selected_m_forward=moment(this.current_year+"-"+month_forward, "YYYY-M")
        days_next_month=this.returnDaysOfMonth(selected_m_forward,this.current_year,month_forward);
        days_next_month.forEach((_day:Day)=>{_day.not_in_curent_month=true});
      }

      for (var _i = 0; _i < selected_m_forward.daysInMonth()-1  ; _i++) { 
        this.days_in_month.push(days_next_month[_i])
        if(days_next_month[_i].name==="Sun"){
          break;
        }
      }
    }
    console.log(this.days_in_month)

    //razdelim mesec na tedne - sklepam da se vsak teden začne s ponedeljkom!

    for (var _i = 0, _j = 6; _j < this.days_in_month.length ; _i = _i+7, _j = _j+7) { 
      this.weeks_in_month.push(this.days_in_month.slice(_i,_j+1))
    }
    console.log(this.weeks_in_month)
  }


  

}

export class Day{
  public date:string='';
  public name:string='';
  public index:number=0;
  public not_in_curent_month:boolean=false;

  constructor(_index:number,_date:string,_name:string){
    this.date=_date;
    this.name=_name;
    this.index=_index;
  }
}
