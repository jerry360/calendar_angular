import { Injectable } from '@angular/core';
import * as moment from 'moment';

import * as fileHolidays from '../../assets/holidays.json'

@Injectable({
  providedIn: 'root'
})

export class HolidayService {

  public holidays:Array<Holiday>=new Array();

  constructor() {
    this.holidays=fileHolidays //schrodingerjev array - je array in ni array
  }


  //tale metoda mi je u sramoto ker sem zelo hitel in mi je tale array nagajal
  public returnHolidayForDay(_date:string):Holiday[]|Array<any>{
    if (this.holidays == undefined || this.holidays == null) {
      return new Array();
    }else{
      let tmpHolidays:Array<Holiday>=new Array();
      for(let i=0;i<this.holidays.length;i++){
        tmpHolidays.push(this.holidays[i])
      }
      let tmpArray:Array<Holiday>=new Array();
      for(let i=0;i<tmpHolidays.length;i++){
        if(tmpHolidays[i].date===_date){
          tmpArray.push(tmpHolidays[i])
          tmpHolidays.splice(i,1)
        }
      }

      for(let i=0;i<tmpHolidays.length;i++){
        if(tmpHolidays[i].repeat){
          if(moment(_date,'YYYY-MM-D').format('MM-D')===moment(tmpHolidays[i].date,'YYYY-MM-D').format('MM-D')){
            let tmpHoli:Holiday = tmpHolidays[i]
            tmpHoli.date=moment(tmpHolidays[i].date,'YYYY-MM-D').year(moment(_date,'YYYY-MM-D').year()).format('YYYY-MM-D')
            tmpArray.push(tmpHoli)
          }
        }
      }
      //return this.holidays.filter((_holiday:Holiday)=>{return (_holiday.date==_date)}) 
      return tmpArray
    }
  }

}
export class Holiday{
  name:string='';
  date:string='';//YYYY-MM-D
  day_off:boolean=false;
  repeat:boolean=false;
}